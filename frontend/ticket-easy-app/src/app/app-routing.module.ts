import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './components/main-page/main-page.component';
import { PurchaseTicketsComponent } from './purchase-tickets/purchase-tickets.component';
import { ProfileComponent } from './components/profile/profile.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './services/auth/auth-guard.service';

const routes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'callback', component: MainPageComponent },
  { path: 'purchase', component: PurchaseTicketsComponent, 
    canActivate: [AuthGuard]
  },
  { path: 'profile', component: ProfileComponent },
  { path: 'login', component: LoginComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
