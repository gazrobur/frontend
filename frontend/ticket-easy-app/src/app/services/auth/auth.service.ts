import { Injectable, Inject } from '@angular/core';
import { environment } from '../../../environments/environment.prod'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  isLoggedIn() {
    return this.http.get(`${this.baseUrl}/user/loggedIn`, {
      withCredentials: true,
      observe: 'response'
    }).toPromise();
  }

  login(loginData: { email: string, password: string }) {
    return this.http.post(`${this.baseUrl}/auth/login`, loginData, { 
      withCredentials: true,
      observe: 'response'
    }).toPromise();
  }

  logout() {
    return this.http.post(`${this.baseUrl}/auth/logout`, { 
      withCredentials: true,
      observe: 'response'
    }).toPromise();
  }

}
