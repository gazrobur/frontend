import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
      public authService: AuthService,
      public router: Router
    ) {}

  async canActivate() {
    const logged = await this.authService.isLoggedIn();
    if (!logged.body) {
        this.router.navigate(['login']);
        return false;
    }
    return true;
  }
}