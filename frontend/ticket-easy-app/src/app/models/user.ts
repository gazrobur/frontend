export class User {
    id: string;
    email: string;
    full_name: string;
    is_admin: boolean;
    status: boolean;
}
