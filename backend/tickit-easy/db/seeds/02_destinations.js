exports.seed = function(knex, Promise) {
  return knex('destinations').del()
    .then(function () {
      return knex('destinations').del()
    })
    .then(function () {
      knex.raw('ALTER TABLE ' + 'destinations' + ' AUTO_INCREMENT = 1');
      const destinations = [];
      const cities = ['Budapest', 'Kecskemét', 'Nagykanizsa', 'Páty', 'Bábolna'];

      for (let i = 0; i < 5; ++i) {
        destinations.push({
          name: cities[i],
        })
      }

      return knex('destinations').insert(destinations);
    })
};
