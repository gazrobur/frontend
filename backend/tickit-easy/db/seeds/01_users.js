// const faker = require('faker');

exports.seed = function(knex, Promise) {
  return knex('users').del()
    .then(function () {
      return knex.raw('TRUNCATE waiting_list, ticket_category, destinations, users RESTART IDENTITY');
    })
    .then(function () {
      const users = []
      users.push({
        full_name: 'Ivan Benedek',
        email: 'bivan@inf.elte.hu',
        password: '$2y$12$3sJGumIdVX/D2E3qdpT6d.oEI6Vb.YoJ8q1ywKxy8gpPhRNINMfVS',
        status: 1,
        is_admin: 1
      });
      // for (let i = 0; i < 30; ++i) {
      //   users.push({
      //     firstName: faker.name.firstName(),
      //     lastName: faker.name.lastName(),
      //     email: faker.internet.email(),
      //     password: '$2y$12$3sJGumIdVX/D2E3qdpT6d.oEI6Vb.YoJ8q1ywKxy8gpPhRNINMfVS',
      //     status: faker.random.arrayElement([0, 1])
      //   })
      // }

      return knex('users').insert(users);
    })
};