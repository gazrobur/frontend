exports.seed = function(knex, Promise) {
    return knex('ticket_category').del()
      .then(function () {
        return knex('ticket_category').del()
      })
      .then(function () {
        const ticket_category = []
        const prices = [3499, 1999, 2999, 2499, 3999];

        for (let i = 1; i <= 5; ++i) {
          ticket_category.push({
            destination: i,
            default_price: prices[i]
          })
        }
        return knex('ticket_category').insert(ticket_category);
      })
  };
  