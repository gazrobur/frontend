
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('permissions', function (table) {
        table.increments('id').primary();
        table.string('name', 30);
        
        table.unique('id');
    })
    .createTable('user_permissions', function (table) {
      table.integer('user_id', 11).unsigned();
      table.integer('permission_id', 11).unsigned();

      table.foreign('user_id').references('id').inTable('users');
      table.foreign('permission_id').references('id').inTable('permissions');
    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('permissions').dropTable('user_permissions')
};
