
exports.up = function(knex, Promise) {
  return knex.schema.table('users', table => {
    table.dropColumn('isAdmin');
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.table('users', table => {
    table.boolean('isAdmin');
  })
};
