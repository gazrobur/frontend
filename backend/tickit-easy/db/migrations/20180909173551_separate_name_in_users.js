
exports.up = function(knex, Promise) {
  return knex.schema.table('users', table => {
    table.dropColumn('fullName');
  }).then(() => {
    return knex.schema.table('users', table => {
      table.string('firstName', 30);
      table.string('lastName', 30);
    });
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.table('users', table => {
    table.string('fullName', 60);
  }).then(() => {
    return knex.schema.table('users', table => {
      table.dropColumn('firstName');
      table.dropColumn('lastName');
    });
  })
};
