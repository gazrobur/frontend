
exports.up = function(knex, Promise) {
  return knex.schema.table('users', table => {
    table.renameColumn('full_name', 'fullName')
    table.renameColumn('is_admin', 'isAdmin')
  })
}

exports.down = function(knex, Promise) {
  return knex.schema.table('users', table => {
    table.renameColumn('fullName', 'full_name')
    table.renameColumn('isAdmin', 'is_admin')
  })
}
