# nodebus

### IFI-CROSS - Booking Bus Ticket (BTT)

#### Getting Started

##### Requirements: 

- nodejs https://nodejs.org/en/
- npm https://www.npmjs.com/

##### Setting up:

- Start server: `npm start`
- Create migration: `npm run migrate:make <table_name>`
- Migrate database: `npm run migrate:latest`
- Rollback migration `npm run migrate:rollback`
- Seed database: `npm run seed:run`

##### Run test:

- Running tests: `npm run test`

##### Docker:

You can also use Docker to run the application, just make sure you have a `.env` file with the following settings:

- MYSQL_HOST
- MYSQL_DATABASE
- MYSQL_ROOT_USER
- MYSQL_ROOT_PASSWORD
- MYSQL_USER
- MYSQL_PASSWORD

If so, just run `docker-compose up`, the application will be available on the following ports:
- node server - **8080**
- mysql - **8989**
- phpmyadmin - **8081**

Run mariadb in docker

`docker run -p 3306:3306 --name mariadb -e MYSQL_ROOT_PASSWORD=password -d mariadb:latest`

##### Database:

Set up your `.env` with the required parameters:

###### Database
- DB_HOST=localhost
- DB_NAME=tabor
- DB_USER=*****
- DB_PASS=*****

###### App
- APP_PORT=3000

###### Examples:
Migration (`<timestamp>_<table_name>.js`):

~~~~
 exports.up = function(knex, Promise) {
        return Promise.all([
            knex.schema.createTable('posts', function(table) {
                table.increments('id').primary();
                table.string('username');
                table.string('password');
                table.string('name');
                table.string('email');
                table.timestamps();
                table.unique('id');
            }),
        ])
    
    };

    exports.down = function(knex, Promise) {
        return Promise.all([
            knex.schema.dropTable('posts')
        ])
    };
~~~~