interface IBaseUser {
  full_name: string;
  email: string;
  password: string;
}

interface IUser extends IBaseUser {
  id?: string;
  status: boolean;
  is_admin: boolean;
}

interface ILoginUser {
  email: string;
  password: string;
}

class User {
  id: string;
  email: string;
  full_name: string;
  is_admin: boolean;
  status: boolean;

  constructor(userObj: IUser) {
    this.id = userObj.id;
    this.email = userObj.email;
    this.full_name = userObj.full_name;
    this.is_admin = userObj.is_admin;
    this.status = userObj.status;
  }
}

export { IBaseUser, IUser, User, ILoginUser };
