interface ITicket extends ITicketCategory, IDestinations {
  id: number;
  ticketNumber: number;
  categoryId: number;
  price: number;
  comment: string;
  isSold: boolean;
  isAvailable: boolean;
}

interface ITicketCategory {
  id: number;
  destination: number;
  defaultPrice: number;
}

interface IDestinations {
  id: number;
  name: string;
}

export { ITicket, ITicketCategory, IDestinations };
