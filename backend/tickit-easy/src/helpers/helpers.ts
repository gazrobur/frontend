import * as web from 'advanced-controllers';

const isSuccess = (ticket: [number]) => {
  const [success] = ticket;
  if (!success) {
    throw new web.WebError('Failed to update ticket!', 400);
  }
  return !!success;
};

export { isSuccess };
