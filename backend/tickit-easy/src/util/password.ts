import * as bcrypt from 'bcryptjs';

const hashPassword = async (password: string): Promise<string> => {
  const salt = await bcrypt.genSalt(13);
  const hash = await bcrypt.hash(password, salt);

  return hash;
};

const isPasswordCorrect = async (
  password: string,
  hash: string
): Promise<boolean> => {
  return bcrypt.compare(password, hash);
};

export { hashPassword, isPasswordCorrect };
