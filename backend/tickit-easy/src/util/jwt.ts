import * as jwt from 'jsonwebtoken';
import * as web from 'advanced-controllers';
import * as dotenv from 'dotenv';
dotenv.config();
const APP_AUTH_KEY = process.env.APP_AUTH_KEY;
const APP_AUTH_COOKIE = process.env.APP_AUTH_COOKIE;
const DAY = 24 * 60 * 60 * 1000;

const signToken = (payload: any, options?: any): Promise<string> => {
  return new Promise((resolve, reject) => {
    jwt.sign(payload, APP_AUTH_KEY, options, (error, token) => {
      if (error) {
        reject(error);
      } else {
        resolve(token);
      }
    });
  });
};

const decodeToken = (token): Promise<any> => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, APP_AUTH_KEY, (error, decoded) => {
      if (error) {
        reject(error);
      } else {
        resolve(decoded);
      }
    });
  });
};

const addAuthCookie = (res: web.Response, token: string): Date => {
  res.header('Authorization', 'Bearer '+ token);
  res.cookie(APP_AUTH_COOKIE, token, {
    httpOnly: true,
    maxAge: 30 * DAY
  });
  const now = new Date();
  now.setDate(now.getDate() + 30); // expiration

  return now;
};

const removeAuthCookie = (res: web.Response): void => {
  res.clearCookie(APP_AUTH_COOKIE);
};

export { signToken, decodeToken, addAuthCookie, removeAuthCookie };
