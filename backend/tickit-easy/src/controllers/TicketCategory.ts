import * as web from 'advanced-controllers';
import knex from '../server/db';
import { ITicketCategory } from '../model/Ticket';

export function isSuccess(ticket: [number]) {
  const success = [ticket]
  if (!success) {
    throw new web.WebError('Failed to update ticket!', 400);
  }
  return !!success;
}
@web.Controller('/ticket-category')
@web.Authorize()
export class TicketCategoryController extends web.AdvancedController {
  @web.Get('/all')
  async getAllTicketCategories(): Promise<ITicketCategory[]> {
    try {
      const ticketCategories: ITicketCategory[] = await knex()('ticket_category').returning('*');
      return ticketCategories;
    } catch (error) {
      throw new web.WebError('Failed to get ticket categories!', 500);
    }
  }

  @web.Post('/create')
  @web.Permission('Admin')
  async createTicketCategory(@web.Body() ticketCategory: ITicketCategory): Promise<boolean> {
    try {
        const [newTicketCategory] = await knex()('ticket_category').insert({ ...ticketCategory });
        return !!newTicketCategory;
    } catch (error) {
      throw new web.WebError('Failed to create ticket category!');
    }
  }

  @web.Put('/update/:id')
  @web.Permission('Admin')
  async updateUser(
    @web.Param('id') id: number, @web.Body() ticketCategory: ITicketCategory): Promise<boolean> {
    const res = await knex()('ticket_category')
      .where('id', id)
      .update(ticketCategory);

    return !!res;
  }

  @web.Del('/delete/:id')
  @web.Permission('Admin')
  async deleteTicketCategory(@web.Param('id') id: number): Promise<boolean> {
    const res = await knex()('ticket_category')
      .where('id', id)
      .del();
    return !!res;
  }
}
