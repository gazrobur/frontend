import * as web from 'advanced-controllers';
import knex from '../server/db';
import { IDestinations } from '../model/Ticket';

@web.Controller('/destination')
@web.Authorize()
export class DestinationController extends web.AdvancedController {
  @web.Get('/all')
  async getAllDestination(): Promise<IDestinations[]> {
    try {
      const destinations: IDestinations[] = await knex()('destinations').returning('*');
      return destinations;
    } catch (error) {
      console.log(error);
      throw new web.WebError('Table destinations does not exists!', 500);
    }
  }

  @web.Get('/:id')
  async getDestinationById(@web.Param('id') id: number): Promise<IDestinations> {
    try {
      const [res] = await knex()('destinations').where('id', id);
      return res;
    } catch (error) {
      throw new web.WebError(`Destination with id ${id} does not exists!`, 500);
    }
  }

  @web.Put('/update/:id')
  @web.Permission('Joker')
  async updateDestination(
    @web.Param('id') id: number, @web.Body() destination: IDestinations): Promise<boolean> {
        try {
            const res = await knex()('destinations')
              .where('id', id)
              .update(destination);
            return !!res;
        } catch(error) {
            throw new web.WebError('Failed to update destination!');
        }
  }

  @web.Post('/create')
  @web.Permission('Admin')
  async createDestination(@web.Body() destination: IDestinations): Promise<boolean> {
    try {
        const [newDestination] = await knex()('destinations').insert({ ...destination });
        return !!newDestination;
    } catch (error) {
      throw new web.WebError('Failed to create destination!');
    }
  }

  @web.Del('/delete/:id')
  @web.Permission('Admin')
  async deleteDestination(@web.Param('id') id: number): Promise<boolean> {
      try {
          const res = await knex()('destinations')
            .where('id', id)
            .del();
          return !!res;
      } catch(error) {
          throw new web.WebError('Failed to delete destination!');
      }
  }
}
